const outdent = require('outdent')({ newline: ' ' })

module.exports = (fileName, format, alt) => {
  return outdent`
    <picture>
      <source media="(min-width: 1px)" 
        srcset="/img/webp/wide/${fileName}.webp" type="image/webp">

      <source media="(min-width: 1px)"
        srcset="/img/originals/wide/${fileName}.${format}">

      <img src="/img/originals/wide/${fileName}.${format}" loading="lazy" alt="${alt}">
    </picture>`
}