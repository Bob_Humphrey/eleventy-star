
const dateMMMDYYYY = require('./plugins/date-MMMDYYYY.filter')

const sitemap = require("eleventy-plugin-sitemap")

module.exports = {
  markdownTemplateEngine: "njk"
}


module.exports = function (eleventyConfig) {

  eleventyConfig.setTemplateFormats([
    "md",
    "njk",
    "html",
    "css",
    "jpg",
    "png"
  ])

  eleventyConfig.addPassthroughCopy("img/originals/full")
  eleventyConfig.addPassthroughCopy("img/originals/half")
  eleventyConfig.addPassthroughCopy("img/originals/misc")
  eleventyConfig.addPassthroughCopy("img/originals/quarter")
  eleventyConfig.addPassthroughCopy("img/originals/three-quarter")
  eleventyConfig.addPassthroughCopy("img/resized/full")
  eleventyConfig.addPassthroughCopy("img/resized/half")
  eleventyConfig.addPassthroughCopy("img/resized/misc")
  eleventyConfig.addPassthroughCopy("img/resized/quarter")
  eleventyConfig.addPassthroughCopy("img/resized/three-quarter")
  eleventyConfig.addPassthroughCopy("img/webp/full")
  eleventyConfig.addPassthroughCopy("img/webp/half")
  eleventyConfig.addPassthroughCopy("img/webp/misc")
  eleventyConfig.addPassthroughCopy("img/webp/quarter")
  eleventyConfig.addPassthroughCopy("img/webp/three-quarter");
  eleventyConfig.addPassthroughCopy("img/webp/wide");

  eleventyConfig.addPassthroughCopy('css')

  eleventyConfig.addPassthroughCopy("robots.txt")

  eleventyConfig.addPassthroughCopy("favicon.ico")

  //   eleventyConfig.addPassthroughCopy('articles');

  //   eleventyConfig.addPassthroughCopy('projects');

  //   eleventyConfig.addPassthroughCopy('work');

  eleventyConfig.addFilter("dateMMMDYYYY", dateMMMDYYYY)

  eleventyConfig.addShortcode("img", require("./plugins/img.shortcode"))
  eleventyConfig.addShortcode("wideimg", require("./plugins/wideimg.shortcode"))

  //   eleventyConfig.addPlugin(syntaxHighlight);

  eleventyConfig.addPlugin(sitemap, {
    sitemap: {
      hostname: "https://star.bob-humphrey.com",
    },
  })

  // eleventyConfig.addCollection("novelsAlpha", function (collection) {
  //   return collection.getFilteredByGlob("novels/*.njk").sort(function (a, b) {
  //     let nameA = a.data.title.toUpperCase()
  //     let nameB = b.data.title.toUpperCase()
  //     if (nameA < nameB) return -1
  //     else if (nameA > nameB) return 1
  //     else return 0
  //   })
  // })

  // eleventyConfig.addCollection("storiesAlpha", function (collection) {
  //   return collection.getFilteredByGlob("stories/*.njk").sort(function (a, b) {
  //     let nameA = a.data.title.toUpperCase()
  //     let nameB = b.data.title.toUpperCase()
  //     if (nameA < nameB) return -1
  //     else if (nameA > nameB) return 1
  //     else return 0
  //   })
  // })

  return {
    passthroughFileCopy: true
  }

}
