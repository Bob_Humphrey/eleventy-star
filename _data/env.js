const environment = process.env.ELEVENTY_ENV
const PROD_ENV = 'production'
const prodUrl = 'https://star.bob-humphrey.com'
const devUrl = 'http://localhost:8080'
const baseUrl = environment === PROD_ENV ? prodUrl : devUrl
const isProd = environment === PROD_ENV
const currentYear = new Date().getFullYear()

module.exports = {
  title: 'My Traveling Star',
  author: 'Bob Humphrey',
  description: 'A photo diary',
  environment,
  isProd,
  baseUrl,
  currentYear
}